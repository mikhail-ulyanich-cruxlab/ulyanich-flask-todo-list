import uuid
from functools import wraps

from flask import current_app, request, after_this_request, \
                  abort, make_response


def generate_anticsrf_token():
    return str(uuid.uuid4())


def ensure_anticsrf_cookie(view):
    """View decorator ensuring it will send or refresh anticsrf cookie"""
    @wraps(view)
    def wrapped_view(*args, **kwargs):
        config = current_app.config.get_namespace("CSRF_COOKIE_")
        COOKIE_MAX_AGE = config["age"]
        COOKIE_NAME = config["name"]

        # generate new token if it's unset or empty
        token = request.cookies.get(COOKIE_NAME) or generate_anticsrf_token()

        @after_this_request
        def set_anticsrf_token(response):
            response.set_cookie(COOKIE_NAME, token, max_age=COOKIE_MAX_AGE)
            return response
        return view(*args, **kwargs)
    return wrapped_view


def csrf_exempt(view):
    view.csrf_exempt = True
    return view


def is_safe_http_method(method: str):
    assert method.isupper()
    return method in ("GET", "HEAD", "OPTIONS", "TRACE")
    # https://tools.ietf.org/html/rfc7231#section-4.2.1


def protect_from_csrf(app):
    COOKIE_NAME = app.config.setdefault("CSRF_COOKIE_NAME", 'csrftoken')
    HEADER_NAME = app.config.setdefault("CSRF_HEADER_NAME", 'X-CSRFToken')
    app.config.setdefault("CSRF_COOKIE_AGE", 30 * 60)

    @app.before_request
    def validate_anticsrf_token():
        view = app.view_functions.get(request.endpoint)
        if not getattr(view, 'csrf_exempt', False) \
           and not is_safe_http_method(request.method):
            header_token = request.headers.get(HEADER_NAME)
            cookie_token = request.cookies.get(COOKIE_NAME)
            if header_token is None or header_token != cookie_token:
                abort(make_response(
                    "Missed or mismatching anticsrf tokens. "
                    f"Be sure to set {HEADER_NAME} HTTP header to "
                    f"the value of '{COOKIE_NAME}' cookie", 400))
