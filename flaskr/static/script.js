const CSRF_COOKIE_NAME = 'csrftoken';
const CSRF_HEADER_NAME = 'X-CSRFToken';
// must be consistent with backend configuration


// Utilities

function my_fetch(url, init) {
    console.log(`my_fetch '${url}'`);

    init.headers = new Headers(init.headers);
    init.headers.append(CSRF_HEADER_NAME, Cookies.get(CSRF_COOKIE_NAME));
    init.credentials = 'same-origin';

    return fetch(url, init).catch(error => {
        console.error("Fetch error: " + error);
        throw error;
    });
}


function form2json(form) {
    // https://stackoverflow.com/a/46774073
    let object = {};
    for(const [key, value] of new FormData(form)) {
        object[key] = value;
    }
    return JSON.stringify(object);
}


function postFormAsJson(form, url) {
    return my_fetch(url, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: form2json(form),
    });
}


// Error handling

function PANIC(message) {
    console.log(message);
    alert(message);
    throw new Error(message);
}


function unexpectedResponse(response) {
    console.error(response);
    PANIC("Unexpected response, probably API has changed");
}


// API actions

function resetForm(form) {
    form.reset();
    for(let input of form.querySelectorAll('input[type="date"]')) {
        input.valueAsDate = new Date();  // today
    }
}


function showResponse(response, comment=response.statusText) {
    response.text().then(text => `${comment}: ${text}`).then(alert);
}


function createItem(form) {
    postFormAsJson(form, '/api/tasks').then(response => {
        if(response.status === 201) {
            resetForm(form);
            refreshTodoList();  // TODO: append instead of refetching
        } else if(response.status === 400) {
            showResponse(response);
        } else {
            unexpectedResponse(response);
        }
    });
}


function setStatus(id, is_done) {
    my_fetch(`/api/tasks/${id}/set_${is_done ? 'done' : 'undone'}`, { method: 'POST' });
}


Node.prototype.removeNode = function() { this.parentNode.removeChild(this) };


function deleteItem(id) {
    my_fetch(`/api/tasks/${id}`, { method: 'DELETE' }).then(response => {
        if(response.ok) {
            document.getElementById(`item${id}`).parentNode.removeNode()
        } else {
            showResponse(response, 'Failed to delete item');
        }
    });
}


// Auth

function login(form) {
    postFormAsJson(form, '/auth/login').then(response => {
        if(response.ok) {
            location.assign('/');
        } else if(response.status === 400 || response.status == 401) {
            showResponse(response);
        } else {
            unexpectedResponse(response);
        }
    });
}


function logout() {
    my_fetch('/auth/logout', { method: 'POST' }).then(response => location.reload());
}


function register(form) {
    postFormAsJson(form, '/auth/register').then(response => {
        if(response.ok) {
            location.assign('/');
        } else if(response.status === 400) {
            // registration failed
            showResponse(response);
        } else {
            unexpectedResponse(response);
        }
    });
}


// User interface

function refreshTodoList(query_params=location.search) {
    my_fetch('/api/tasks' + query_params, {method: 'GET'}).then(response => {
        if (response.ok) {
            return response.json();
        } else {
            unexpectedResponse(response);
        }
    }).then(json_todo_list => {
        let todo_list = document.getElementById('todo-list');
        if(json_todo_list.length === 0) {
            todo_list.innerHTML = '<p>There is no any todo items. Add one!</p>';
        } else {
            console.log(json_todo_list);
            let concatenated_li = json_todo_list.map(todo_item => {
                return `
<li>
    <input type="checkbox" id="item${todo_item.id}"
           onclick="setStatus(${todo_item.id}, event.target.checked)"
           ${todo_item.is_done ? "checked" : ""} />

    <label for="item${todo_item.id}">
        ${todo_item.text}
    </label>
    <span class="date">${todo_item.date}</span>
    <button type="button" class="delete-button"
            onclick="deleteItem(${todo_item.id})">&#x274C;</button>
    <!--span class="owner">${todo_item.username}</span--> <!-- FUN -->

</li> `;
            }).join("");
            todo_list.innerHTML = `<ul class="todo-list"> ${concatenated_li} </ul>`;
        }
    });
}


function updateSortByLinks(sortby, reverse) {
    console.log(`updateSortByLinks(${sortby}, ${reverse})`);
    for(let existing_key of ['state', 'date']) {
        let new_href = `?sortby=${existing_key}`;
        let new_reverse = ((!reverse) && existing_key === sortby);
        if(new_reverse)
            new_href += '&reverse';

        for(let link of document.querySelectorAll(`a.sortby${existing_key}`)) {
            link.href = new_href;
            link.onclick = event => {
                event.preventDefault();
                refreshListPage(existing_key, new_reverse);
                return false;
            }
        }
    }
}


function refreshListPage(sortby, reverse) {
    let query_string = '';
    if(sortby) {
        if(sortby !== 'state' && sortby !== 'date') {
            PANIC(`sortby must be 'state' or 'date', not ${sortby}`);
        }

        query_string = `?sortby=${sortby}`;
        if(reverse)
            query_string += '&reverse';
    }

    history.pushState(data=undefined, title="", url=query_string || location.pathname);

    updateSortByLinks(sortby, reverse);
    refreshTodoList(query_string);
}
