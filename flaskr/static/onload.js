window.searchParams = new URLSearchParams(location.search.substr(1));

{
    let sortby = searchParams.get('sortby');
    let reverse = searchParams.has('reverse');

    document.addEventListener("DOMContentLoaded", event => {
        //alert("DOM fully loaded and parsed");
        refreshListPage(sortby, reverse);
    });
}

resetForm(document.getElementById('add_task_form'));
    
for(let link of document.querySelectorAll('a.unsort')) {
    link.onclick = event => {
        event.preventDefault();
        refreshListPage(undefined, undefined);
        return false;
    }
}
