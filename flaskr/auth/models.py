import flask_user
from flask import current_app as app

from ..models import db, Model


class User(Model, flask_user.UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(), nullable=False, unique=True)
    password = db.Column(db.String(), nullable=False)

    @classmethod
    def create_user(cls, username, password, **kwargs):
        return cls.create(username=username,
                          password=app.user_manager.hash_password(password),
                          **kwargs)

    def verify_password(self, password):
        return app.user_manager.verify_password(password, self.password)
