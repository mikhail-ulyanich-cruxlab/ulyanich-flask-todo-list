import flask
import flask_login
import sqlalchemy.exc
import trafaret as t

from .models import User


bp = flask.Blueprint('auth', __name__, url_prefix='/auth')


@bp.route('/register', methods=['POST'])
def register():
    try:
        data = t.Dict(username=t.String(min_length=4),
                      password=t.String).check(flask.request.json)
    except t.DataError as e:
        return str(e.as_dict()), 400
    username = data['username']
    password = data['password']

    try:
        user = User.create_user(username, password)
    except sqlalchemy.exc.IntegrityError:
        return "Username is already taken", 400

    flask_login.login_user(user)
    return '', 201


@bp.route('/login', methods=['POST'])
def login():
    try:
        data = t.Dict(username=t.String, password=t.String).check(
            flask.request.json
        )
    except t.DataError as e:
        return str(e.as_dict()), 400
    username = data['username']
    password = data['password']

    user = User.query.filter_by(username=username).one_or_none()
    if user is not None and user.verify_password(password):
        flask_login.login_user(user)
        return '', 204
    else:
        return 'Invalid username or password', 401


@bp.route('/logout', methods=['POST'])
def logout():
    flask_login.logout_user()
    return '', 204
