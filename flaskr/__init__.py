import datetime

import flask
from flask_sqlalchemy import SQLAlchemy
from flask_user import UserManager

from .csrf import protect_from_csrf

app = flask.Flask(__name__)
app.config.from_pyfile('config.py')

protect_from_csrf(app)


class DateJsonEncoder(flask.json.JSONEncoder):
    def default(self, o):
        if hasattr(o, '__json__'):
            return o.__json__()
        elif isinstance(o, datetime.date):
            return o.isoformat()
        else:
            return super().default(o)


app.json_encoder = DateJsonEncoder


db = SQLAlchemy(app)

from . import auth
app.register_blueprint(auth.views.bp)
UserManager(app, db, auth.models.User)

from . import web_interface
app.register_blueprint(web_interface.views.bp)

from . import api
app.register_blueprint(api.views.bp)

db.create_all()
