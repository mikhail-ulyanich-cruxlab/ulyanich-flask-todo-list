import datetime

import trafaret as t


class Date(t.Trafaret):
    def __repr__(self):
        return "<Date>"

    def check_and_return(self, value):
        if isinstance(value, datetime.date):
            return value
        elif isinstance(value, str):
            try:
                return datetime.datetime.strptime(value, '%Y-%m-%d').date()
            except ValueError as e:
                self._failure(error=str(e))
