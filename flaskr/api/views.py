import flask
from flask.views import MethodView
import flask_login
import trafaret as t

from . import models
from .utils import SingleObjectMixin, SingleObjectJsonView, ListJsonView
from .models import TodoItem
from . import trafarets


bp = flask.Blueprint('api', __name__, url_prefix='/api')


class OwnedTodoItemsMixin(object):
    model = TodoItem

    def get_queryset(self):
        if flask_login.current_user.is_authenticated:
            return flask_login.current_user.todoitems
        else:
            # return self.model.query  # FUN
            return self.model.query.filter_by(owner=None)


class TodoItemJsonView(OwnedTodoItemsMixin, SingleObjectJsonView):
    pass


class TodoItemChangeView(OwnedTodoItemsMixin, SingleObjectMixin, MethodView):
    def set_is_done(self, is_done):
        assert isinstance(is_done, bool)
        obj = self.get_object()
        obj.is_done = is_done
        obj.save()
        return '', 204

    def post(self, *, is_done, **kwargs):
        return self.set_is_done(is_done)


bp.add_url_rule('/tasks/<int:pk>', view_func=TodoItemJsonView.as_view('task'))

bp.add_url_rule('/tasks/<int:pk>/set_done',
                view_func=TodoItemChangeView.as_view('set_done'),
                defaults=dict(is_done=True))
bp.add_url_rule('/tasks/<int:pk>/set_undone',
                view_func=TodoItemChangeView.as_view('set_undone'),
                defaults=dict(is_done=False))


def get_authenticated_user_or_none(user):
    return user if user.is_authenticated else None


class TasksView(OwnedTodoItemsMixin, ListJsonView):
    def get_ordering(self):
        try:
            sortby = t.Enum(None, 'state', 'date').check(
                flask.request.args.get('sortby')
            )
        except t.DataError as e:
            flask.abort(flask.make_response(str(e.as_dict()), 400))
        else:
            if sortby is None:
                return None
            elif sortby == 'state':
                sortby = self.model.is_done
            else:  # sortby == 'date'
                sortby = self.model.date

        reverse = 'reverse' in flask.request.args
        return models.db.desc(sortby) if reverse else sortby

    def post(self):
        try:
            data = t.Dict(text=t.String, date=trafarets.Date).check(
                flask.request.json
            )
        except t.DataError as e:
            return str(e.as_dict()), 400
        text = data['text']
        date = data['date']

        user = get_authenticated_user_or_none(flask_login.current_user)
        flask.current_app.logger.debug("Creting todo item owned by %s", user)
        item = self.model.create(text=text, date=date, owner=user)

        response = flask.make_response("", 201)
        response.headers['Location'] = item.get_absolute_url()
        return response


bp.add_url_rule('/tasks', view_func=TasksView.as_view('tasks'))
