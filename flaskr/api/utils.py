import flask
import flask.views


class ModelMixin(object):
    model = None

    def get_list(self):
        return self.get_queryset().order_by(self.get_ordering()).all()

    def get_queryset(self):
        return self.model.query

    def get_ordering(self):
        return None


class SingleObjectMixin(ModelMixin):
    def get_object(self):
        assert 'pk' in flask.request.view_args
        pk = flask.request.view_args['pk']
        return self.get_queryset().filter_by(id=pk).first_or_404()


class SingleObjectJsonView(SingleObjectMixin, flask.views.MethodView):
    def get(self, *args, **kwargs):
        return flask.jsonify(self.get_object())

    def delete(self, *args, **kwargs):
        self.get_object().delete()
        return '', 204


class ListJsonView(ModelMixin, flask.views.MethodView):
    def get(self):
        return flask.jsonify(self.get_list())
