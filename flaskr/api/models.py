import datetime

from flask import url_for

from ..models import db, Model
from ..auth.models import User


class TodoItem(Model):
    id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.String)
    is_done = db.Column(db.Boolean, default=False)
    date = db.Column(db.Date, default=datetime.date.today)
    owner_id = db.Column(db.ForeignKey(User.id))
    owner = db.relationship('User',
                            backref=db.backref('todoitems', lazy='dynamic'))

    def get_absolute_url(self):
        return url_for('api.task', pk=self.id)

    # FUN
    # def __json__(self):
        # json = super().__json__()
        # json['username'] = None if self.owner is None else self.owner.username
        # return json
