import flask

from ..auth.models import User
from ..csrf import ensure_anticsrf_cookie


bp = flask.Blueprint('ui', __name__)


@bp.route('/', methods=['GET'])
@ensure_anticsrf_cookie
def index():
    return flask.render_template('list.html')


@bp.route('/login', methods=['GET'])
@ensure_anticsrf_cookie
def login():
    return flask.render_template('login.html')


@bp.route('/users/', methods=['GET'])
def users():
    return flask.render_template('users.html', user_list=User.query.all())
