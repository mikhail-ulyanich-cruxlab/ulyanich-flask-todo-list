SECRET_KEY = ' the veeee' + 'e'*25 + 'eeery random string'
SQLALCHEMY_DATABASE_URI = 'sqlite:///../db.sqlite'
USER_ENABLE_EMAIL = False
SQLALCHEMY_TRACK_MODIFICATIONS = False

CSRF_COOKIE_NAME = 'csrftoken'
CSRF_COOKIE_AGE = 30 * 60
CSRF_HEADER_NAME = 'X-CSRFToken'
