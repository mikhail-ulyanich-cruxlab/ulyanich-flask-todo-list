from sqlalchemy import inspect

from . import db


class Model(db.Model):
    __abstract__ = True

    def __json__(self):
        return {
            column: getattr(self, column)
            for column in inspect(self).mapper.column_attrs.keys()
        }

    def __repr__(self):
        return f"{self.__class__.__name__}{self.__json__()!r}"

    def delete(self):
        db.session.delete(self)
        db.session.commit()
        return self

    def save(self):
        db.session.add(self)
        db.session.commit()
        return self

    @classmethod
    def create(cls, *args, **kwargs):
        return cls(*args, **kwargs).save()
