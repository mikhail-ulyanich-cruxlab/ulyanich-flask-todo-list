from setuptools import setup, find_packages
setup(
    name="ulianich-flask-todolist",
    version="0.1",
    packages=find_packages(),
    install_requires=['flask', 'flask_sqlalchemy', 'sqlalchemy', 'flask_user',
                      'trafaret'],
    author="IT'S ME, MARIO",
)
